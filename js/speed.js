function Speed(vx, vy) {
	this.x = Math.round(vx * 100) / 100;
	this.y = Math.round(vy * 100) / 100;

	this.add = function(speed) {
		this.x += speed.x;
		this.y += speed.y;
	}

	this.toString = function() {
		return "{Speed: x: " + this.x + ", y: " + this.y + "}";
	}
}