function Life(max) {
	var max = max;
	var current = max;

	var observers = new Array();

	this.registerObserver = function(observer) {
		observers.push(observer)
	}

	this.notifyDeath = function() {
		for (var i = 0; i < observers.length; ++i) {
			observers[i].notifyDeath();
		}
	}

	this.kill = function() {
		current = 0;
		this.notifyDeath();
	}

	this.lower = function(quantity) {
		current -= quantity;
		if (current <= 0)
			this.kill();
	}

	this.current = function() {
		return current;
	}
	this.max = function() {
		return max;
	}

	this.dead = function() {
		return current <= 0;
	} 
}