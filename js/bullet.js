function Bullet(pos, speed, size) {
	this.pos = new Position(pos.x, pos.y);
	this.speed = new Speed(speed.x, speed.y);
	this.size = size;
	this.id = Bullet.id++;

	this.life = new Life(1);
	this.life.registerObserver(this);

	var observers = new Array();

	this.registerObserver = function(observer) {
		observers.push(observer);
	}

	this.notifyDeath = function() {
		for (var i = 0; i < observers.length; ++i) {
			observers[i].notifyDeathBullet(this);
		}
	}

	this.move = function(deltaT) {
		this.pos.x += speed.x * deltaT;
		this.pos.y += speed.y * deltaT;
	}

	this.distance = function(bullet) {
		return this.pos.distance(bullet.pos);
	}

	this.toString = function() {
		var output = "{Bullet: " + pos.toString();
		output += ", " + speed.toString();
		output += ", size: " + size + "}";
		return output;
	}

	this.explode = function(targets) {
		for (var i = 0; i < targets.length; ++i) {
			targets[i].life.lower(this.size);
		}
		this.life.kill();
	}
}
Bullet.id = 0;