function Canon(pos, size) {
	// Variables globales
	var BULLET_SPEED = 1000,
		BULLET_SIZE = size / 2;


	this.pos = pos;
	this.size = size;

	this.life = new Life(1000);

	this.angle = Math.PI/2;

	this.fire = function() {
		return new Bullet(
			new Position(Math.cos(this.angle) * size * 4, Math.sin(this.angle) * size * 4),
			new Speed( Math.cos(this.angle) * BULLET_SPEED, Math.sin(this.angle) * BULLET_SPEED ),
			BULLET_SIZE
		)
	}

	this.toString = function() {
		var output = "Canon: [";
		output += "]";
		return output;
	}

}