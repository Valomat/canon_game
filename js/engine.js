function Engine() {
	/*
		Informations about the time of the engine.
	*/
	var DELTA_T = 0; // real time space between to beats
	var TIME = 0; // current time
	var MAX_TIME = 60; // The number of seconds after which TIME is reset

	var WIDTH; // dimension x of the simulation
	var HEIGHT; // dimension y of the simulation

	var CANON_SIZE = 20;

	// Canon of the game
	this.canon = new Canon( new Position(0, 0), CANON_SIZE );

	// Bullets fired
	this.bullets = new Array();
	// Ennemy bullets
	this.ennemies = new Array();
	// Objects that are observing the simulation
	observers = new Array();

	/*
		Method to register observers to the simulation
	*/
	this.registerObserver = function(observer) {
		observers.push(observer);
	}

	this.notifyDeathBullet = function(bullet) {
		for (var i = 0; i < this.ennemies.length; ++i) {
			if (this.ennemies[i] === bullet)
				this.removeEnnemy(i);
		}
		for (var i = 0; i < this.bullets.length; ++i) {
			if (this.bullets[i] === bullet)
				this.removeBullet(i);
		}
	}
	this.notifyDeathPlayer = function(bullet) { }

	/*
		Notify observers when a bullet is created
	*/
	function notifyCreateBullet(bullet) {
		for (var i = 0; i < observers.length; ++i) {
			observers[i].notifyCreateBullet(bullet);
		}
	}

	/*
		Notify observers when an ennemy bullet is created
	*/
	function notifyCreateEnnemy(bullet) {
		for (var i = 0; i < observers.length; ++i) {
			observers[i].notifyCreateEnnemy(bullet);
		}
	}


	/*
		Notify observers when a bullet is removed
	*/
	function notifyRemoveBullet(bullet) {
		for (var i = 0; i < observers.length; ++i) {
			observers[i].notifyRemoveBullet(bullet);
		}
	}

	/*
		Notify observers when a bullet ennemy is being removed
	*/
	function notifyRemoveEnnemy(bullet) {
		for (var i = 0; i < observers.length; ++i) {
			observers[i].notifyRemoveEnnemy(bullet);
		}
	}

	/*
		Notify when an ennemy is killed
	*/
	function notifyDestroyEnnemy(bullet) {
		for (var i = 0; i < observers.length; ++i) {
			observers[i].notifyDestroyEnnemy(bullet);
		}
	}

	/*
		Notify when player dies
	*/
	function notifyDeathPlayer() {
		for (var i = 0; i < observers.length; ++i) {
			observers[i].notifyDeathPlayer();
		}
	}

	/*
		Create a new bullet
	*/
	this.createBullet = function() {
		var bullet = this.canon.fire();
		bullet.registerObserver(this);
		this.bullets.push(bullet);
		notifyCreateBullet(bullet);
	}

	/*
		Create a new ennemy bullet
	*/
	this.createEnnemy = function() {
		var bullet = new Bullet(
			new Position(Math.random() * WIDTH - WIDTH / 2, HEIGHT),
			new Speed(0 , -250), 
			15
			);
		bullet.registerObserver(this);
		this.ennemies.push(bullet);
		notifyCreateEnnemy(bullet);
	}

	/*
		Remove an ennemy bullet
	*/
	this.removeEnnemy = function(i) {
		var bullet = this.ennemies[i];
		this.ennemies.splice(i, 1);
		notifyRemoveEnnemy(bullet);
	}

	/*
		Remove a bullet
	*/
	this.removeBullet = function(i) {
		var bullet = this.bullets[i];
		this.bullets.splice(i, 1);
		notifyRemoveBullet(bullet);
	}

	/*
		Change the time space between two beats
	*/
	this.setDeltaT = function(deltaT) {
		DELTA_T = deltaT;
	}

	/*
		Set the dimensions of the simulation
	*/
	this.setDimensions = function(w, h) {
		WIDTH = w;
		HEIGHT = h;
	}

	/*
		Event to do on mouseMove of the graphic interface
	*/
	this.mouseMoveEvent = function(x, y) {
		this.canon.angle = Math.atan2(y - this.canon.pos.y, x - this.canon.pos.x)
	}

	/*
		Event to do on mouseClick of the graphic interfarce
	*/
	this.mouseClickEvent = function(x, y) {
		this.canon.angle = Math.atan2(y - this.canon.pos.y, x - this.canon.pos.x)
		this.createBullet();
	}

	/*
		Core method. This method is called at every beat of th game clock.
		It manages the simulation of the game.
	*/
	this.core = function() {

		if ( this.canon.life.dead() ) {
			notifyDeathPlayer();
		}

		/*
			For each bullet
		*/
		for (var i = 0; i < this.bullets.length; ++i) {
			// If the bullet is dead or outside the boundaries
			if ( !inMap(this.bullets[i]) ) {
				this.bullets[i].life.kill();
				continue;
			}

			// Move current bullet
			this.bullets[i].move(DELTA_T);

			/*
				Check ennemy collision
			*/
			for (var j = 0; j < this.ennemies.length; ++j) {
				// If collision
				if ( this.bullets[i].distance( this.ennemies[j] ) < this.bullets[i].size + this.ennemies[j].size ) {
					// Kill ennemy and current bullet
					this.bullets[i].explode([this.ennemies[j]])
					notifyDestroyEnnemy(this.ennemies[j]);
					break;
				}
			}
		}

		/*
			manage ennemy motion
		*/
		for (var i = 0; i < this.ennemies.length; ++i) {
			// If the ennemy bullet is dead or outside the boundaries
			if ( !inMap(this.ennemies[i]) ) {
				this.ennemies[i].explode([this.canon]);
				continue;
			}
			
			this.ennemies[i].move(DELTA_T);
		}


		/*
			TODO better ennemy creation
		*/
		if (Math.random() < 0.02)
			this.createEnnemy();

				
		// Update time information
		TIME = (TIME + DELTA_T) % 10

	}

	/*
		Return true if obj is inside the engine dimensions. In terms of position x, y.
	*/
	function inMap( obj ) {
		return (obj.pos.x > -WIDTH/2 - obj.size) && (obj.pos.x < WIDTH/2 + obj.size) &&
			(obj.pos.y > 0 - obj.size) && (obj.pos.y < HEIGHT + obj.size)
	}
}