function Graphic(canvasId, deltaT) {
	Graphic.c = document.getElementById(canvasId);
	var c = Graphic.c;
	var ctx = c.getContext("2d");
	ctx.translate(c.width/2, c.height);

	var engine;	

	var img = new Image();
	img.src = "img/fond.jpg";

	var DELTA_T = deltaT;

	var gCanon = null;


	this.notifyCreateBullet = function(bullet) {
		graphicBullet.bullets.push( new graphicBullet(bullet, "#c20b0b", DELTA_T) );
	}
	this.notifyCreateEnnemy = function(bullet) {
		graphicBullet.bullets.push( new graphicBullet(bullet, "#5a5a5a", DELTA_T) );
	}
	this.notifyRemoveBullet = function(bullet) {
		var index = this.findBullet(bullet);
		if (index >= 0)
			graphicBullet.bullets[index].deathTrigger();
	}
	this.notifyRemoveEnnemy = function(bullet) {
		this.notifyRemoveBullet(bullet);
	}
	this.findBullet = function(bullet) {
		for (var i = 0; i < graphicBullet.bullets.length; ++i) {
			if (graphicBullet.bullets[i].bullet() === bullet)
				return i;
		}
		return -1;
	}
	this.notifyDestroyEnnemy = function(bullet) { }
	this.notifyDeathPlayer = function(bullet) { 
		gCanon.deathTrigger();
	}

	this.setEngine = function(refEngine) {
		engine = refEngine;
		engine.setDimensions(c.width, c.height);
		engine.registerObserver(this);
		gCanon = new graphicCanon(engine.canon, DELTA_T);
	}

	this.core = function() {
		ctx.clearRect(-c.width/2, -c.height, c.width, c.height);
		ctx.drawImage(
			img,
			-c.width/2, 
			-c.height,
			c.width,
			c.width / 1.778);
		ctx.closePath();
		for (var i = 0; i < graphicBullet.bullets.length; ++i) {
			graphicBullet.bullets[i].draw(ctx);
			if (graphicBullet.bullets[i].isFinished()) {
				graphicBullet.bullets.splice(i, 1);
				i--;
			}
		}
		gCanon.draw(ctx);
	}



	$("#canvas").mousemove(function(e){
		var x = e.clientX - c.width/2;
		var y = Graphic.c.height -e.clientY;

		engine.mouseMoveEvent(x, y);
	})

	$("#canvas").click(function(e){
		var x = e.clientX - c.width/2;
		var y = Graphic.c.height -e.clientY;

		engine.mouseClickEvent(x, y);
	})

}
Graphic.c = null;

function graphicCanon(canon, deltaT) {
	var canon = canon;
	var DELTA_T = deltaT;

	this.explosionsAnimation = new Array();

	this.deathTrigger = function() {
		for (var i = 0; i < 1; ++i) {
			this.explosionsAnimation.push(
				new Animation(
					graphicBullet.explosion, 
					this.drawExplosion, 
					0.5, 
					DELTA_T
				)
			)
		}
	}

	this.drawExplosion = function(ctx, img, currentTime) {
		ctx.drawImage(
			img,
			canon.pos.x - canon.size * 4, 
			-canon.pos.y - canon.size * 4,
			canon.size * 8,
			canon.size * 8
		);
	}

	this.isFinished = function() {

		return this.explosionsAnimation.length > 0 && this.explosionsAnimation[0].finished();
	}

	this.draw = function(ctx) {
		ctx.fillStyle = 'black';
		ctx.translate(canon.pos.x, canon.pos.y);
		ctx.rotate(-canon.angle);
		ctx.drawImage(
			graphicCanon.img,
			0, 
			-canon.size * 1.2 / 2, 
			canon.size * 4.4, 
			canon.size * 1.2);
		ctx.rotate(canon.angle);
		ctx.translate(-canon.pos.x,-canon.pos.y);

		ctx.beginPath();
		ctx.arc(canon.pos.x, -canon.pos.y, canon.size, 0, 2 * Math.PI, false);
		ctx.fillStyle = 'grey';
		ctx.fill();
		ctx.closePath();

		if ( !this.isFinished() )
			for (var i = 0; i < this.explosionsAnimation.length; ++i) {
				this.explosionsAnimation[i].anim(ctx);
			}
	}	
}
graphicCanon.img = new Image();
graphicCanon.img.src = "img/canon.png";

function graphicBullet(bullet, color, deltaT) {
	var bullet = bullet;
	var color = color;
	var DELTA_T = deltaT;

	this.explosionAnimation = null;

	this.isFinished = function() {

		return this.explosionAnimation != null && this.explosionAnimation.finished();
	}

	this.deathTrigger = function() {
		this.explosionAnimation = new Animation(graphicBullet.explosion, this.drawExplosion, 0.5, DELTA_T);
	}

	this.bullet = function() {
		return bullet;
	}

	this.draw = function(ctx) {
		ctx.beginPath();
		ctx.arc(bullet.pos.x, -bullet.pos.y, bullet.size, 0, 2 * Math.PI, false);
		ctx.fillStyle = color;
		ctx.fill();
		ctx.closePath();
		ctx.drawImage(
			graphicBullet.img,
			bullet.pos.x - bullet.size, 
			-bullet.pos.y - bullet.size,
			bullet.size * 2,
			bullet.size * 2);

		if (this.explosionAnimation != null) {

			this.explosionAnimation.anim(ctx);
			
		}
	}

	this.drawExplosion = function(ctx, img, currentTime) {
		ctx.drawImage(
			img,
			bullet.pos.x - bullet.size * 3, 
			-bullet.pos.y - bullet.size * 3,
			bullet.size * 6,
			bullet.size * 6
		);
	}
}
graphicBullet.img = new Image();
graphicBullet.img.src = "img/bullet.png";
graphicBullet.bullets = new Array();

graphicBullet.explosion = new Array();
for (var i = 0; i < 14; ++i) {
	var img = new Image();
	img.src = "img/explosion/explosion00" + i + ".png";
	graphicBullet.explosion.push(img);
}
