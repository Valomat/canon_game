function Game() {

	var DELTA_T = 1/50;
	var BEAT = 0;

	var heart;

	var graphic;

	var engine = new Engine();
	engine.setDeltaT(DELTA_T);
	engine.registerObserver(this);

	var score = 0;
	var $scorePanel = null;
	var $lifePanel = null;


	this.notifyCreateBullet = function(bullet) { }
	this.notifyCreateEnnemy = function(bullet) { }
	this.notifyRemoveBullet = function(bullet) { }
	this.notifyRemoveEnnemy = function(bullet) { }
	this.notifyDestroyEnnemy = function(bullet) {
		score += 50;
	}

	var hasEnded = false;
	this.notifyDeathPlayer = function() { 
		var g = this;
		if (!hasEnded) {
			hasEnded = true;
			setTimeout(function() {
				console.log("END")
				g.pause();
				$("#score").removeClass("hidden");
			}, 500);
		}
	}

	this.setCanvas = function(canvasID) {
		graphic = new Graphic(canvasID, DELTA_T);
		graphic.setEngine(engine);
	}

	function gameRound() {
		engine.core();
		graphic.core();

		if (BEAT % 10 === 0)
			ui();

		BEAT = (BEAT + 1) % (1 / DELTA_T);
	}

	function ui() {
		if ($scorePanel != null) {
			$scorePanel.html(score);
		}
		if ($lifePanel != null) {
			$lifePanel.find(".current").css("width", engine.canon.life.current() / engine.canon.life.max() * 100 + "%");
		}
	}

	this.setScorePanel = function($container) {
		$scorePanel = $container;
	}
	this.setLifePanel = function($container) {
		$lifePanel = $container
	}

	this.start = function() {
		heart = setInterval(function() {
			gameRound();
		}, DELTA_T * 1000);
	}
	this.pause = function() {
		console.log("pause")
		clearInterval(heart);
	}
}

