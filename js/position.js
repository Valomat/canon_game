function Position(x, y) {
	this.x = Math.round(x * 100) / 100;
	this.y = Math.round(y * 100) / 100;

	/*
		Return distance between to positions.
	*/
	this.distance = function(pos) {
		var a = this.x - pos.x;
		var b = this.y - pos.y;
		return Math.sqrt(  a * a + b * b );
	}

	this.toString = function() {
		return "{Position: x: " + this.x + ", y: " + this.y + "}";
	}
}