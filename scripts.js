var game;

$(document).ready(function() {

	var width = $(window).width();
	var height = $(window).height();

	if (width / height < 1.778) {
		$("#canvas").attr("width", $(window).width() )
		$("#canvas").attr("height", parseInt( $(window).width() / 1.778 ) )

		$(".screen").css("width", $(window).width() )
		$(".screen").css("height", parseInt( $(window).width() / 1.778 ) )
	} else {
		$("#canvas").attr("width", parseInt($(window).height() * 1.778) )
		$("#canvas").attr("height", parseInt( $(window).height() ) )

		$(".screen").css("width", parseInt($(window).height() * 1.778) )
		$(".screen").css("height", parseInt( $(window).height() ) ) 
	}

})



$(".start").bind('click', {  }, function(e) {
	game = new Game();
	game.setCanvas("canvas");
	game.setScorePanel( $(".score") )
	game.setLifePanel( $(".life") )
	game.start();

	$("#home").addClass("hidden")
	$("#score").addClass("hidden")
});
$(".redo").bind('click', {  }, function(e) {
	location.reload();
});